# Backend assignment
An assignment to be solved when applying for a backend developer job/internship at Improving.

## The assignment
Write PHP code to import products to a Magento installation. The products data is supplied in a CSV-file.

You need to:

* Install a clean version of Magento Comunity Edition 2.x.
* Create the missing attributes (fields) in Magento, manually or by code.
* Import the products from the CSV-file into Magento, by creating a custom module/script.

We encourage you to complete the assigment, by creating a Magento module, but it is not required.

## Requirements
Magento CE - Download the free Community Edition of Magento at https://magento.com/products/community-edition

## Setup
Clone the project to your computer or make a fork here at Bitbucket.
```
git clone https://bitbucket.org/weimprove/backend-assignment.git
```
## Local development environment
Need a local development environment, to do the work? Take a look at these:

* For Mac users - https://www.mamp.info/en/
* For Windows users - http://www.wampserver.com/en/

## Apply to the job!
We look forward to your application. Send us a link to your solution (including source files).